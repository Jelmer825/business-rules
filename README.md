# README #

### What is this repository for? ###

* Recommendation rules

### Recommendation rules ###

* Een rule-based systeem ten behoeve van de recommendation engine.

### Algoritmes ###

#### Collaborative ##### 
* Voor de collaborative recommendation heb ik alle profiles opgehaald die een recommendation hebben.
* Vervolgens categoriseer ik elk profiel gebaseerd op het recommendation segment. 
* Hierna pak ik van elke categorie 2 profielen met een producten die eerder zijn bekeken door de personen.
* Vervolgens zoek ik hierbij 4 producten die lijken op de bekeken producten gebaseerd op de sub_category.
* Hierna wordt het profiel_id samen met de 4 producten toegevoegd aan de nieuwe tabel. 

#### Content ##### 
* Voor de content recommendation heb ik alle orders gepakt.
* Ik zet alle orders om naar een dictionary.
* Per order pak ik nu 1 product en zoek ik wederom 4 producten die daarop lijken.
* Vervolgens voeg ik een product_id samen met de 4 product_ids die lijken op het product toe aan de nieuwe tabel.


