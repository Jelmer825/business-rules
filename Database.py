import psycopg2

class Database():
    def __init__(self):
        self.database = "analyse_database"
        self.user = "postgres"
        self.password = "gc6501"
        self.conn = None
        self.cur = None

    def connect(self):
        """
        Opens connection with database and sets cursor
        :return void:
        """
        self.conn = psycopg2.connect(f"dbname={self.database} user={self.user} password={self.password}")
        self.cur = self.conn.cursor()

    def post(self, query):
        """
        Executes a 'post' query
        :param query:
        :return:
        """
        # Connect
        self.connect()

        # Execute query
        self.cur.execute(query)
        self.conn.commit()

        # Close connection
        self.close()

    def get(self, query):
        """
        Executes a 'get' query
        :param query:
        :return:
        """
        try:
            # Connect
            self.connect()

            # Execute query
            self.cur.execute(query)

            results = self.cur.fetchall()

            # Close connection
            self.close()

            return results
        except psycopg2.Error:
            print(f"Skipped due to syntax error: {query}")

    def close(self):
        """
        Closes connection with database
        :return void:
        """
        self.cur.close()
        self.conn.close()

    def tableExists(self, tableName):
        """
        Check if a table exists
        :param tableName:
        :return bool:
        """

        query = """
            SELECT EXISTS (
               SELECT FROM information_schema.tables 
               WHERE  table_schema = 'schema_name'
               AND    table_name   = 'table_name'
               );
             """
        pass