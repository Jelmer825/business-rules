from Database import Database
class Rules():

    def __init__(self):
        self.database = Database()
        self.start()

    def start(self):
        """
        Start program
        :return void:
        """
        self.getContent()
        self.getCollaborative()

    def createCollaborativeTable(self):
        """
        Create the collaborative table
        :return void:
        """
        query = """CREATE TABLE IF NOT EXISTS collaborative_filter(
                    profile_id VARCHAR(255),
                    similar_1 VARCHAR(255),
                    similar_2 VARCHAR(255),
                    similar_3 VARCHAR(255),
                    similar_4 VARCHAR(255)
                )"""

        self.database.post(query)

    def seedCollaborativeTable(self, products):
        """
        Seed the collaborative table
        :param products:
        :return void:
        """

        query = f"""INSERT INTO collaborative_filter (profile_id,
                                            similar_1,
                                            similar_2,
                                            similar_3,
                                            similar_4) VALUES('{products[0][0]}', '{products[0][1][0][0]}', '{products[0][1][1][0]}', '{products[0][1][2][0]}', '{products[0][1][3][0]}')"""

        self.database.post(query)

    def getCollaborative(self):
        """
        Get all profiles with recommendation segment and products viewed before
        :return void:
        """
        query = """SELECT huengine_profile.id as profile_id, 
            huengine_recommendation.segment, 
            huengine_product.id as product_id
            FROM huengine_recommendation
            INNER JOIN huengine_profile ON huengine_recommendation.profile_id = huengine_profile.id
            INNER JOIN huengine_viewedbefore ON huengine_viewedbefore.recommendation_id = huengine_recommendation.id
            INNER JOIN huengine_viewedbefore_products ON huengine_viewedbefore_products.viewedbefore_id = huengine_viewedbefore.id
            INNER JOIN huengine_product ON huengine_viewedbefore_products.product_id = huengine_product.id"""

        # Result tuple of query
        res = self.database.get(query)

        # Results
        results = self.createSimilars(res)

        # Make table
        self.createCollaborativeTable()

        # Loop through results and create similar products for a profile
        for key in results:
            for value in results[key]:
                product = self.getSimilarProducts("id", value[1])
                if product:
                    fourSimilars = [[value[0], self.getSimilarProducts("sub_category", product[0][-3])]]
                    self.seedCollaborativeTable(fourSimilars)

    def createSimilars(self, res):
        """
        Create a dictionary categorized on segment
        :param res:
        :return dictionary:
        """

        dict = {
            "BOUNCER": [

            ],
            "JUDGER": [

            ],
            "COMPARER": [

            ],
            "LEAVER": [

            ],
            "BUYER": [

            ],
            "BROWSER": [

            ]
        }

        # Categorize on recommendation segment
        for tuple in res:
            split = [tuple[0], tuple[2]]
            if split not in dict[tuple[1]]:
                dict[tuple[1]].append([tuple[0], tuple[2]])

        return dict

    def createContentTable(self):
        """
        Create the content table
        :return void:
        """
        query = """CREATE TABLE IF NOT EXISTS content_filter(
            product_id VARCHAR(255),
            similar_1 VARCHAR(255),
            similar_2 VARCHAR(255),
            similar_3 VARCHAR(255),
            similar_4 VARCHAR(255)
        )"""

        self.database.post(query)

    def seedContentTable(self, products):
        """
        Seed the content table
        :param products:
        :return void:
        """
        query = f"""INSERT INTO content_filter (product_id, 
                                            similar_1, 
                                            similar_2, 
                                            similar_3, 
                                            similar_4) VALUES({products[0]}, {products[1]}, {products[2]}, {products[3]}, {products[4]})"""

        self.database.post(query)

    def getContent(self):
        """
        Content filtering
        :return void:
        """

        """
            Select per order all products sold 
        """
        query = """SELECT huengine_orders_products_count.order_id, huengine_orders_products_count.product_amount, huengine_product.id, 
                huengine_product.category, 
                huengine_product.sub_category,
                huengine_product.sub_sub_category,
                huengine_product.gender,
                huengine_price.selling_price
                FROM huengine_orders_products_count
                INNER JOIN huengine_product ON huengine_product.id = huengine_orders_products_count.product_id
                INNER JOIN huengine_price ON huengine_product.price_id = huengine_price.id"""

        # Order result after query
        orderRes = self.database.get(query)

        # Result to dict
        orders = self.orderToDict(orderRes)

        similars = []

        # Get first order (in this case) and get 4 similar products
        for order_id in orders:
            for productDict in orders[order_id]['products']:
                for product_id in productDict:
                    similarTuples = self.getSimilarProducts('sub_category', productDict[product_id]['sub_category'])
                    similars.append(product_id)
                    for tuple in similarTuples:
                        similars.append(tuple[0])
            break

        # Create and seed table
        self.createContentTable()
        self.seedContentTable(similars)

    def orderToDict(self, res):
        """
        Convert every order to a dictionary
        :param res:
        :return dictionary:
        """

        columns = ["order_id", "product_amount", "product_id", "category",
                  "sub_category", "sub_sub_category", "gender", "selling_price"]

        ordersDict = {}

        for row in res:
            orderId = row[0]

            # Temp dict order
            tempDict = {
                "products": []
            }

            # Temp dict product
            productDict = {
                f"{row[2]}": {
                    "product_amount": 0,
                    "category": "",
                    "sub_category": "",
                    "sub_sub_category": "",
                    "gender": "",
                    "selling_price": 0
                    }
            }

            # Loop through every value in product tuple and add to dict
            for value in row:
                index = row.index(value)
                if index != 0 and index != 2:
                    productDict[row[2]][columns[index]] = value

            if orderId in ordersDict:
                ordersDict[orderId]['products'].append(productDict)
            else:
                tempDict['products'].append(productDict)
                ordersDict[orderId] = tempDict
        return ordersDict

    def productToDict(self, res):
        """
        Convert a product tuple to a dict
        :param res:
        :return dictinonary:
        """
        productDict = {}

        for row in res:
            listRow = list(row)
            productId = listRow[0]
            listRow.pop(0)
            productDict[productId] = listRow

        return productDict

    def getProducts(self):
        """
        Get all products
        :return list:
        """
        productQuery = f"""SELECT * FROM huengine_product"""
        productRes = self.database.get(productQuery)
        return productRes

    def getSimilarProducts(self, where, like):
        """
        Get products with where and like statement.
        :param where:
        :param like:
        :return:
        """
        productQuery = f"""SELECT * FROM huengine_product WHERE {where} LIKE '{like}' LIMIT 4"""
        productRes = self.database.get(productQuery)
        return productRes